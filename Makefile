.PHONY: all test clean debug

FLAGS = -std=c99 -Wall

MAIN_PROGRAM_FILES = main.c parsers/algo_parser.c sokoban_game/solution_t.c queue/queue.c sokoban_game/game_functions.c sokoban_game/solver.c parsers/init_state_parser.c sokoban_game/game_structures.c sokoban_game/solution_action_list.c printer/printer.c sokoban_game/repeated_states.c min_heap/min_heap.c heuristics/heuristics.c

TEST_1_FILES = tests/game_function_tests/game_function_tests.c tests/game_function_tests/test_main.c sokoban_game/game_functions.c sokoban_game/game_structures.c sokoban_game/solution_action_list.c

TEST_2_FILES = tests/solution_actions_list_tests/test_main.c tests/solution_actions_list_tests/solution_actions_list_test.c sokoban_game/game_structures.c sokoban_game/game_functions.c sokoban_game/solution_action_list.c 

sokoban-solver: $(MAIN_PROGRAM_FILES)
	gcc $(FLAGS) -o sokoban-solver.out $(MAIN_PROGRAM_FILES) -lm

test: $(TEST_1_FILES) $(TEST_2_FILES)
	gcc $(FLAGS) -o test_game_functions.out $(TEST_1_FILES)
	gcc $(FLAGS) -o test_solution_actions_list.out $(TEST_2_FILES)

clean:
	find . -type f -name '*.gch' -delete
	find . -type f -name '*.out' -delete

debug:
	gcc $(FLAGS) -g $(MAIN_PROGRAM_FILES) -o sokoban-solverG.out -lm

all: sokoban-solver
