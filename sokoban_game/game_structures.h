#ifndef __GAME_STRUCTURES__

#define __GAME_STRUCTURES__

#include "solution_action_list.h"

typedef enum game_object_type {
    WALL_OBJ        = 0,
    NO_OBJ          = 1,
    UNPLACED_BOX    = 2,
    PLACED_BOX      = 3,
    GOAL            = 4,
    PLAYER          = 5,
    NOT_AN_OBJECT   = 6,
} game_object_type_t;

typedef struct
{
    int row;
    int col;
} position_t;

typedef struct
{
    int height;
    int width;
    int objectives;
    int boxes;
} dimensions_t;

typedef enum {
    BOARD_INSUFICIENT_MEMORY,
    BOARD_BAD_ARGUMENTS,
    BOARD_INIT_OK,
} board_issues_t;
/*
 * Enum type for game board position
 * Every game board position is either FREE (meaning
 * the user may move itself or a box to it),
 * a BOX or a WALL
 * */
typedef enum position_state
{
    FREE  = 0,
    WALL  = 1,
} position_state_t;

/*
 * Dynamic state variables associated with a particular
 * game state
 * */
typedef struct dynamic_game_state_t {
    int   player_row;
    int   player_col;
    int   num_boxes;
    int * boxes_rows;
    int * boxes_cols;
} dynamic_game_state_t;

/*
 * Static state variables associated with a particular
 * game state
 * */
typedef struct static_game_state_t {
    position_state_t ** state_matrix;
    int                 rows;
    int                 cols;
    int                 num_objectives;
    int *               objective_rows;
    int *               objective_cols;
} static_game_state_t;

/*
 * Main structure that represents a game state.
 * Association of a dynamic game state and a
 * static game state
 * */
typedef struct game_state
{
    dynamic_game_state_t * dynamic_game_state;
    static_game_state_t  * static_game_state;
} game_state_t;

dynamic_game_state_t * copy_dynamic_game_state(dynamic_game_state_t * dynamic_info);

int states_equals(dynamic_game_state_t * g1, dynamic_game_state_t * g2);

int init_board(game_state_t * game_state, dimensions_t * dimensions);

int place_in_board(game_state_t * game_state, position_t * position, game_object_type_t type);

int add_box(game_state_t * game_state, position_t * position, game_object_type_t type);

int add_objective(game_state_t * game_state, position_t * position, game_object_type_t type);

int add_player(game_state_t * game_state, position_t * position, game_object_type_t type);

int move_board_next_row(game_state_t * game_state, position_t * position, game_object_type_t type);

game_object_type_t get_item_at_pos(game_state_t * game_state, position_t * position);

int player_at_boundaries(static_game_state_t * static_info, int row, int col);

int box_at(dynamic_game_state_t * state, int row, int col);

int wall_at(static_game_state_t * state, int target_row, int target_col);

int free_at(static_game_state_t * state, int target_row, int target_col);

#endif
