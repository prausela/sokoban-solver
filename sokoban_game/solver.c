#include "game_structures.h"
#include "game_functions.h"
#include "solver.h"
#include "solution_t.h"
#include "../queue/queue.h"
#include "../min_heap/min_heap.h"
#include "../printer/printer.h"
#include "../min_heap/min_heap.h"
#include "repeated_states.h"
#include <stdio.h>
#include <stdlib.h>

#define NEW_MIN_NOT_SET -1
#define DELTA_DEPTH 50

static int (*curr_heuristic)(game_state_t*);

static int heap_f_function(void *);
static int heap_heuristic(void *);

static int dfs(game_state_t * initial_state, solution_actions_list * action_list);
static int bfs(game_state_t * initial_state, solution_actions_list * action_list);
static int a(game_state_t * initial_state, solution_actions_list * action_list);
static int global_greedy(game_state_t * initial_state, solution_actions_list * action_list);
static int ida(game_state_t * initial_state, solution_actions_list * action_list);
static int iddfs(game_state_t * curr_state, solution_actions_list * action_list);
static int iddfsRec(game_state_t * curr_state, solution_actions_list * action_list, int depth, int min, int * new_min, int * explored_everything);
static int generic_search(game_state_t * curr_state, solution_actions_list * action_list, min_heap heap);
static int nodes_to_expand(game_state_t * state);

static static_game_state_t * static_info;

solution_actions_list * solve(game_state_t * initial_state, search_algos_id_t algorithm,
    int (*heuristic)(game_state_t*)) {

    /*
    Instantiate return value structure
    */
    solution_actions_list* ret = new_solution_actions_list(INITIAL_LIST_SIZE);
    if(ret == NULL) {
        return ret;
    }

    ret->frontier = 1;
    ret->expanded = 0;

    static_info = initial_state->static_game_state;

    initialize_repeated_states();

    /*
    Set curr_heuristic so as to not pass
    it around all the time
    */
    curr_heuristic = heuristic;

    /*
    Switch over the values of algorithm and select
    the appropriate algorithm
    */
    switch(algorithm){
        case A:
            a(initial_state, ret);
            break;
        case IDA:
            add_state(initial_state->dynamic_game_state);
            ida(initial_state, ret);
            break;
        case GGS:
            global_greedy(initial_state, ret);
            break;
        case DFS:
            add_state(initial_state->dynamic_game_state);
            dfs(initial_state, ret);
            break;
        case BFS:
            bfs(initial_state, ret);
            break;
        case IDDFS:
            add_state(initial_state->dynamic_game_state);
            iddfs(initial_state, ret);
            break;
    }

    return ret;

}

static int dfs(game_state_t * curr_state, solution_actions_list * action_list) {
    dynamic_game_state_t * dynamic_info = curr_state->dynamic_game_state;
    /*
    Iterate over possible actions applicable
    to the current game state.
    */
    int solution_found = 0;
    int nodes          = nodes_to_expand(curr_state);
    action_list->frontier += nodes - 1;
    action_list->expanded += 1;
    for(action_t action = UP; action <= RIGHT && !solution_found; action++) {
        action_result_t result = apply_action(dynamic_info, static_info, action);
        if(result != NOT_MOVED) {
            if(contains_state(dynamic_info)) {
                de_apply_action(dynamic_info, static_info, action, result);
                continue;
            } else {
                add_state(dynamic_info);
            }
            add_action_to_list(action_list, action);
            if(is_solution(dynamic_info, static_info)) {
                solution_found = 1;
            } else {
                solution_found = dfs(curr_state, action_list);
                if(!solution_found) {
                    de_apply_action(dynamic_info, static_info, action, result);
                    remove_recent(action_list);
                }
            }
        }
    }
    if(!solution_found) {
        action_list->frontier += -nodes + 1;
        action_list->expanded -= 1;
    }
    return solution_found;
}

static int bfs(game_state_t * initial_state, solution_actions_list * action_list) {
    /*
    Iterate over the possible game states through
    a breadth-first search using a queue data structure
    */
    queue q = new_queue(sizeof(solution_t));
    solution_t * initial_node = new_solution(initial_state->dynamic_game_state, 1);
    enqueue(q, initial_node);
    action_list->frontier = 1;
    int solution_found = 0;
    int error = 0;
    while(!solution_found && !is_empty(q) && !error) {
        solution_t * curr_solution = poll(q);
        if(contains_state(curr_solution->dynamic_info)) {
            action_list->frontier -= 1;
            continue;
        } else {
            add_state(curr_solution->dynamic_info);
        }
        if(is_solution(curr_solution->dynamic_info, static_info)) {
            /*
            Copy the solution actions list and free up the entire
            structure
            */
            long aux_expanded = action_list->expanded;
            long aux_frontier = action_list->frontier;
            copy_list_into(curr_solution->actions_list, action_list);
            action_list->expanded = aux_expanded;
            action_list->frontier = aux_frontier;
            solution_found = 1;
        } else {
            /*
            Iterate over possible actions and push the solutions
            into the queue
            */
            action_list->expanded += 1;
            action_list->frontier -= 1;
            int children = 0;
            for(action_t action = UP; action <= RIGHT; action++) {
                if(can_apply_action(curr_solution->dynamic_info, static_info, action)) {
                    solution_t * next_solution = apply_action_to_solution(curr_solution, static_info, action);
                    if(next_solution != NULL) {
                        children++;
                        enqueue(q, next_solution);
                        action_list->frontier += 1;
                    } else {
                        error = 1;
                    }
                }
            }
        }
    }
    if(error) return 0;
    return solution_found;
}

static int heap_f_function(void * solution) {
    game_state_t state;
    state.dynamic_game_state = ((solution_t*)solution)->dynamic_info;
    state.static_game_state = static_info;
    int heuristic_value = curr_heuristic(&state);
    if(heuristic_value == -1) {
        return -1;
    }
    int depth = ((solution_t*)solution)->depth;
    return heuristic_value + depth;
}

static int a(game_state_t * initial_state, solution_actions_list * action_list) {
    min_heap heap = new_heap(&heap_f_function);
    return generic_search(initial_state, action_list, heap);
}

static int global_greedy(game_state_t * initial_state, solution_actions_list * action_list) {
    min_heap heap = new_heap(&heap_heuristic);
    return generic_search(initial_state, action_list, heap);
}

static int auxHeuristic(game_state_t * state) {
    return 0;
}

static int iddfs(game_state_t * curr_state, solution_actions_list * action_list) {
    curr_heuristic = &auxHeuristic;
    return ida(curr_state, action_list);
}

static int ida(game_state_t * initial_state, solution_actions_list * action_list) {
    int solution_found = 0;
    int explored_everything = 0;
    int initial_threshold = curr_heuristic(initial_state);
    if(initial_threshold == -1) {
        return 0;
    }
    int min = initial_threshold;
    int new_min = NEW_MIN_NOT_SET;
    add_state(initial_state->dynamic_game_state);
    while(!solution_found && !explored_everything) {
        action_list->frontier = 1;
        action_list->expanded = 0;
        solution_found = iddfsRec(initial_state, action_list, 0, min, &new_min, &explored_everything);
        if(new_min == NEW_MIN_NOT_SET) {
            explored_everything = 1;
        }
        clear_repeated_states();
        add_state(initial_state->dynamic_game_state);
        min = new_min + DELTA_DEPTH;
        new_min = NEW_MIN_NOT_SET;
    }

    return solution_found;
}

static int iddfsRec(game_state_t * curr_state, solution_actions_list * action_list, int depth, int min, int * new_min, int * explored_everything) {
    dynamic_game_state_t * dynamic_info = curr_state->dynamic_game_state;


    int heuristic_value = curr_heuristic(curr_state);
    if(heuristic_value == -1) {
        return 0;
    }

    /*
    Check if threshold was surpassed
    */
    if(depth + heuristic_value > min) {
        /*
        Update if necessary new_min for next
        iddfs calls
        */
        if(*new_min == NEW_MIN_NOT_SET || depth + heuristic_value < *new_min) {
            *new_min = depth + heuristic_value;
        }
        return 0;
    }

    if(is_solution(dynamic_info, static_info)) {
        return 1;
    }
    /*
    Iterate over possible actions applicable
    to the current game state.
    */
    int solution_found = 0;
    int nodes          = nodes_to_expand(curr_state);
    action_list->frontier += nodes - 1;
    action_list->expanded += 1;
    for(action_t action = UP; action <= RIGHT && !solution_found; action++) {
        action_result_t result = apply_action(dynamic_info, static_info, action);
        if(result != NOT_MOVED) {
            if(contains_state(dynamic_info)) {
                de_apply_action(dynamic_info, static_info, action, result);
                continue;
            } else {
                add_state(dynamic_info);
            }
            add_action_to_list(action_list, action);
            solution_found = iddfsRec(curr_state, action_list, depth + 1, min, new_min, explored_everything);
            if(!solution_found) {
                de_apply_action(dynamic_info, static_info, action, result);
                remove_recent(action_list);
            }
        }
    }
    if(!solution_found) {
        action_list->frontier += -nodes + 1;
        action_list->expanded -= 1;
    }
    return solution_found;
}

static int generic_search(game_state_t * curr_state, solution_actions_list * action_list, min_heap heap) {
    int solution_found = 0;
    solution_t * initial = new_solution(curr_state->dynamic_game_state, 1);
    add(heap, initial);
    game_state_t state;
    while(!solution_found && !heap_is_empty(heap)) {
        solution_t * curr_solution = pop(heap);
        if(!contains_state(curr_solution->dynamic_info)) {
            add_state(curr_solution->dynamic_info);
        } else {
            action_list->frontier -= 1;
            continue;
        }
        if(is_solution(curr_solution->dynamic_info, static_info)) {
            long aux_expanded = action_list->expanded;
            long aux_frontier = action_list->frontier;
            copy_list_into(curr_solution->actions_list, action_list);
            action_list->expanded = aux_expanded;
            action_list->frontier = aux_frontier;
            solution_found = 1;
        } else {
            int children = 0;
            action_list->expanded += 1;
            action_list->frontier -= 1;
            for(action_t action = UP; action <= RIGHT; action++) {
                if(can_apply_action(curr_solution->dynamic_info, static_info, action)) {
                    solution_t * next_solution = apply_action_to_solution(curr_solution, static_info, action);
                    if(next_solution != NULL) {
                        children++;
                        add(heap, next_solution);
                        action_list->frontier += 1;
                    }
                }
            }
            state.static_game_state = static_info;
            state.dynamic_game_state = curr_solution->dynamic_info;
        }
    }
    return solution_found;
}

static int nodes_to_expand(game_state_t * state) {
    int nodes = 0;
    for(action_t action = UP; action <= RIGHT; action++) {
        if(can_apply_action(state->dynamic_game_state, state->static_game_state, action)) {
            nodes++;
        }
    }
    return nodes;
}

static int heap_heuristic(void * state) {
    solution_t * aux = (solution_t*)state;
    game_state_t game_state;
    game_state.static_game_state = static_info;
    game_state.dynamic_game_state = aux->dynamic_info;
    return curr_heuristic(&game_state);
}
