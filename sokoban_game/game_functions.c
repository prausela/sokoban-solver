#include "game_functions.h"
#include "game_structures.h"

static int move(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, int delta_row, int delta_col);
static int applicable(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, int delta_row, int delta_col);
static int player_on_top_of_box(dynamic_game_state_t * dynamic_info);
static void move_box(dynamic_game_state_t * state, int box, int delta_row, int delta_col);
static void de_apply_box_movement(dynamic_game_state_t * dynamic_info, int delta_row, int delta_col, action_result_t result);

int apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action) {
    int ret = NOT_MOVED;
    switch(action) {
        case UP:
            ret = move(dynamic_info, static_info, -1, 0);
            break;
        case DOWN:
            ret = move(dynamic_info, static_info, 1, 0);
            break;
        case RIGHT:
            ret = move(dynamic_info, static_info, 0, 1);
            break;
        case LEFT:
            ret = move(dynamic_info, static_info, 0, -1);
            break;
    }
    return ret;
}

void de_apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action, action_result_t result) {
    switch(action) {
        case UP:
            apply_action(dynamic_info, static_info, DOWN);
            de_apply_box_movement(dynamic_info, 1, 0, result);
            break;
        case DOWN:
            apply_action(dynamic_info, static_info, UP);
            de_apply_box_movement(dynamic_info, -1, 0, result);
            break;
        case RIGHT:
            apply_action(dynamic_info, static_info, LEFT);
            de_apply_box_movement(dynamic_info, 0, -1, result);
            break;
        case LEFT:
            apply_action(dynamic_info, static_info, RIGHT);
            de_apply_box_movement(dynamic_info, 0, 1, result);
            break;
    }
}

int is_solution(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info) {
    /*
    Check every row and col in the objective space and see if
    they are filled with boxes
    */
    int num_boxes_found = 0;
    for(int i = 0; i < static_info->num_objectives; i++) {
        int obj_row = static_info->objective_rows[i];
        int obj_col = static_info->objective_cols[i];

        int found_box = 0;
        for(int box = 0; box < dynamic_info->num_boxes && !found_box; box++) {
            if(dynamic_info->boxes_rows[box] == obj_row && dynamic_info->boxes_cols[box] == obj_col) {
                found_box = 1;
                num_boxes_found++;
            }
        }

    }
    /*
     * The game is solved when the number of boxes in objective positions
     * equals the number of boxes
     * */
    return num_boxes_found == static_info->num_objectives;
}

int can_apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action) {
    int ret = 0;
    switch(action) {
        case UP:
            ret = applicable(dynamic_info, static_info, -1, 0);
            break;
        case DOWN:
            ret = applicable(dynamic_info, static_info, 1, 0);
            break;
        case RIGHT:
            ret = applicable(dynamic_info, static_info, 0, 1);
            break;
        case LEFT:
            ret = applicable(dynamic_info, static_info, 0, -1);
            break;
    }
    return ret;
}

static int move(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, int delta_row, int delta_col) {
    if(!applicable(dynamic_info, static_info, delta_row, delta_col)) return 0;
    /*
    Move player to new position
    */
    dynamic_info->player_row += delta_row;
    dynamic_info->player_col += delta_col;
    /*
    If player is on top the box, move the box
    */
    int box = player_on_top_of_box(dynamic_info);
    int ret = NOT_MOVED_BOX;
    if(box >= 0) {
        move_box(dynamic_info, box, delta_row, delta_col);
        ret = MOVED_BOX;
    }
    return ret;
}

static void move_box(dynamic_game_state_t * state, int box, int delta_row, int delta_col) {
    state->boxes_rows[box] += delta_row;
    state->boxes_cols[box] += delta_col;
}

static int applicable(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, int delta_row, int delta_col) {
    /*
     * Check for boundaries
     * */
    if(!player_at_boundaries(static_info, dynamic_info->player_row + delta_row, dynamic_info->player_col + delta_col)) {
        return 0;
    }
    /*
    Check for wall collision
    */
    if(wall_at(static_info, dynamic_info->player_row + delta_row, dynamic_info->player_col + delta_col)) {
        return 0;
    }
    /*
    Check for box-wall collission
    */
    if(box_at(dynamic_info, dynamic_info->player_row + delta_row, dynamic_info->player_col + delta_col) >= 0
            && wall_at(static_info, dynamic_info->player_row + 2*delta_row, dynamic_info->player_col + 2*delta_col)) {
        return 0;
    }
    /*
    Check for box-box collision
    */
    if(box_at(dynamic_info, dynamic_info->player_row + delta_row, dynamic_info->player_col + delta_col) >= 0
            && box_at(dynamic_info, dynamic_info->player_row + 2*delta_row, dynamic_info->player_col + 2*delta_col) >= 0) {
        return 0;
    }
    return 1;
}

static int player_on_top_of_box(dynamic_game_state_t * dynamic_info) {
    return box_at(dynamic_info, dynamic_info->player_row, dynamic_info->player_col);
}

static void de_apply_box_movement(dynamic_game_state_t * dynamic_info, int delta_row, int delta_col, action_result_t result) {
    if(result != MOVED_BOX) return;
    int box = box_at(dynamic_info, dynamic_info->player_row - 2*delta_row, dynamic_info->player_col - 2*delta_col);
    if(box >= 0) {
        move_box(dynamic_info, box, delta_row, delta_col);
    }
}
