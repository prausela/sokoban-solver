#include <stddef.h>
#include <stdlib.h>
#include "game_structures.h"
#include "../parsers/init_state_parse.h"

int init_board(game_state_t * game_state, dimensions_t * dimensions){

    if(game_state == NULL || dimensions == NULL)
        return BOARD_INSUFICIENT_MEMORY;

    game_state->static_game_state                 = malloc(sizeof(static_game_state_t));
    if(game_state->static_game_state == NULL)
        return BOARD_INSUFICIENT_MEMORY;

    game_state->dynamic_game_state                = malloc(sizeof(dynamic_game_state_t));
    if(game_state->dynamic_game_state == NULL)
        return BOARD_INSUFICIENT_MEMORY;

    game_state->static_game_state->cols           = dimensions->width;
    game_state->static_game_state->rows           = dimensions->height;
    game_state->static_game_state->num_objectives = 0;

    game_state->static_game_state->state_matrix   = malloc(dimensions->height * sizeof(position_state_t*));
    if(game_state->static_game_state->state_matrix == NULL)
        return BOARD_INSUFICIENT_MEMORY;

    for (size_t i = 0; i < dimensions->height; i++)
    {
        (game_state->static_game_state->state_matrix)[i] = malloc(dimensions->width * sizeof(position_state_t));
        if((game_state->static_game_state->state_matrix)[i] == NULL)
            return BOARD_INSUFICIENT_MEMORY;
    }
    
    game_state->static_game_state->objective_cols = malloc(dimensions->objectives * sizeof(int));
    if(game_state->static_game_state->objective_cols == NULL)
        return BOARD_INSUFICIENT_MEMORY;
    game_state->static_game_state->objective_rows = malloc(dimensions->objectives * sizeof(int));
    if(game_state->static_game_state->objective_rows == NULL)
        return BOARD_INSUFICIENT_MEMORY;

    game_state->dynamic_game_state->num_boxes     = 0;

    game_state->dynamic_game_state->boxes_cols    = malloc(dimensions->boxes * sizeof(int));
    if(game_state->dynamic_game_state->boxes_cols == NULL)
        return BOARD_INSUFICIENT_MEMORY;
    game_state->dynamic_game_state->boxes_rows    = malloc(dimensions->boxes * sizeof(int));
    if(game_state->dynamic_game_state->boxes_rows == NULL)
        return BOARD_INSUFICIENT_MEMORY;
    return BOARD_INIT_OK;
}

int free_board(game_state_t * game_state){

    for(int i = 0; i < game_state->static_game_state->cols; i++)
        free((game_state->static_game_state->state_matrix)[i]);

    free(game_state->static_game_state->state_matrix);

    free(game_state->static_game_state->objective_cols);
    free(game_state->static_game_state->objective_rows);

    free(game_state->dynamic_game_state->boxes_cols);
    free(game_state->dynamic_game_state->boxes_rows);

    free(game_state->static_game_state);
    free(game_state->dynamic_game_state);

    //Todo: modify or change return type to void
    return 1;
}

int place_in_board(game_state_t * game_state, position_t * position, game_object_type_t type){
    if(game_state == NULL || position == NULL){
        return BOARD_BAD_ARGUMENTS;
    }

    if(type == WALL_OBJ){
        (game_state->static_game_state->state_matrix)[position->row][position->col] = WALL;
    } else if(type == NO_OBJ){
        (game_state->static_game_state->state_matrix)[position->row][position->col] = FREE;
    }

    (position->col)++;

    return BOARD_INIT_OK;
}

int add_box(game_state_t * game_state, position_t * position, game_object_type_t type){

    (game_state->dynamic_game_state->boxes_rows)[game_state->dynamic_game_state->num_boxes] = position->row;
    (game_state->dynamic_game_state->boxes_cols)[game_state->dynamic_game_state->num_boxes] = position->col;
    if(type == PLACED_BOX){
        add_objective(game_state, position, type);
        (position->col)--;
    }

    (game_state->dynamic_game_state->num_boxes)++;

    (position->col)++;

    return BOARD_INIT_OK;
}

int add_objective(game_state_t * game_state, position_t * position, game_object_type_t type){

    (game_state->static_game_state->objective_rows)[game_state->static_game_state->num_objectives] = position->row;
    (game_state->static_game_state->objective_cols)[game_state->static_game_state->num_objectives] = position->col;

    (game_state->static_game_state->num_objectives)++;

    (position->col)++;

    return BOARD_INIT_OK;
}

int add_player(game_state_t * game_state, position_t * position, game_object_type_t type){

    game_state->dynamic_game_state->player_row = position->row;
    game_state->dynamic_game_state->player_col = position->col;
    
    (position->col)++;
    
    return BOARD_INIT_OK;
}

int move_board_next_row(game_state_t * game_state, position_t * position, game_object_type_t type){
    int issue;
    while(position->col < game_state->static_game_state->cols){
        if((issue = place_in_board(game_state, position, NO_OBJ)) != BOARD_INIT_OK)
            return issue;
    }
    (position->row)++;
    position->col = 0;
    return BOARD_INIT_OK;
}

dynamic_game_state_t * copy_dynamic_game_state(dynamic_game_state_t * dynamic_info) {

    dynamic_game_state_t * ret = (dynamic_game_state_t *)malloc(sizeof(dynamic_game_state_t));

    ret->player_col = dynamic_info->player_col;
    ret->player_row = dynamic_info->player_row;

    ret->boxes_cols = (int*)malloc(dynamic_info->num_boxes*sizeof(int));
    ret->boxes_rows = (int*)malloc(dynamic_info->num_boxes*sizeof(int));

    ret->num_boxes  = dynamic_info->num_boxes;

    for(int box = 0; box < dynamic_info->num_boxes; box++) {
        ret->boxes_rows[box] = dynamic_info->boxes_rows[box];
        ret->boxes_cols[box] = dynamic_info->boxes_cols[box];
    }

    return ret;
}

int states_equals(dynamic_game_state_t * g1, dynamic_game_state_t * g2) {
    if(g1->num_boxes != g2->num_boxes) return 0;
    if(g1->player_row != g2->player_row) return 0;
    if(g1->player_col != g2->player_col) return 0;
    int equals = 1;
    for(int box = 0; box < g1->num_boxes && equals; box++) {
        equals = 0;
        for(int box2 = 0; box2 < g2->num_boxes && !equals; box2++) {            
            if(g1->boxes_rows[box] == g2->boxes_rows[box2] && g1->boxes_cols[box] == g2->boxes_cols[box2]) {
                equals = 1;
            }
        }
    }
    return equals;
}

int box_at(dynamic_game_state_t * state, int row, int col) {
    int found = 0;
    int ret = -1;
    for(int box = 0; box < state->num_boxes && !found; box++) {
        if(state->boxes_rows[box] == row && state->boxes_cols[box] == col) {
            found = 1;
            ret = box;
        }
    }
    return ret;
}

int goal_at(static_game_state_t * state, int row, int col) {
    int found = 0;
    int ret = -1;
    for(int goal = 0; goal < state->num_objectives && !found; goal++) {
        if(state->objective_rows[goal] == row && state->objective_cols[goal] == col) {
            found = 1;
            ret = goal;
        }
    }
    return ret;
}

int wall_at(static_game_state_t * state, int target_row, int target_col) {
    if(!player_at_boundaries(state, target_row, target_col)) return 0;
    return state->state_matrix[target_row][target_col] == WALL;
}

int free_at(static_game_state_t * state, int target_row, int target_col) {
    return state->state_matrix[target_row][target_col] == FREE;
}

int player_at(dynamic_game_state_t * state, int target_row, int target_col){
    return state->player_row == target_row && state->player_col == target_col;
}


int player_at_boundaries(static_game_state_t * static_info, int row, int col) {
    return row < static_info->rows && row >= 0
            && col < static_info->cols && col >= 0;
}


game_object_type_t get_item_at_pos(game_state_t * game_state, position_t * position){
    
    if(player_at(game_state->dynamic_game_state, position->row, position->col)){
        return PLAYER;
    } else if (box_at(game_state->dynamic_game_state, position->row, position->col) >= 0){
        if(goal_at(game_state->static_game_state, position->row, position->col) != -1)
            return PLACED_BOX;
        return UNPLACED_BOX;
    } else if (goal_at(game_state->static_game_state, position->row, position->col) != -1) {
        return GOAL;
    } else if (wall_at(game_state->static_game_state, position->row, position->col) != 0){
        return WALL_OBJ;
    } else if (free_at(game_state->static_game_state, position->row, position->col) != 0){
        return NO_OBJ;
    }
    return NOT_AN_OBJECT;
}