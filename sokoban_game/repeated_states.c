#include <stdlib.h>
#include "repeated_states.h"
#include "game_structures.h"

#define BLOCK 256

typedef struct list {
    dynamic_game_state_t ** dyanmic_infos;
    int size;
    int index;
} list;

static list * repeated;

static int increase_array_size();

void initialize_repeated_states() {
    repeated = (list *)malloc(sizeof(list));
    repeated->dyanmic_infos = (dynamic_game_state_t **)malloc(BLOCK*sizeof(dynamic_game_state_t*));
    repeated->size = BLOCK;
    repeated->index = 0;
}

void clear_repeated_states() {
    for(int i = 0; i < repeated->index; i++) {
        free(repeated->dyanmic_infos[i]);
    }
    repeated->index = 0;
}

int add_state(dynamic_game_state_t * dynamic_info) {
    dynamic_game_state_t * to_add = copy_dynamic_game_state(dynamic_info);
    int result = 1;
    if(repeated->index == repeated->size) {
        result = increase_array_size();
    }
    repeated->dyanmic_infos[repeated->index] = to_add;
    repeated->index += 1;
    return result;
}

int contains_state(dynamic_game_state_t * dynamic_info) {
    for(int i = 0; i < repeated->index; i++) {
        if(states_equals(repeated->dyanmic_infos[i], dynamic_info)) {
            return 1;
        }
    }
    return 0;
}

static int increase_array_size() {
    dynamic_game_state_t ** aux = repeated->dyanmic_infos;
    repeated->dyanmic_infos = (dynamic_game_state_t **)malloc((repeated->size + BLOCK)*sizeof(dynamic_game_state_t *));
    if(repeated->dyanmic_infos == NULL) {
        return 0;
    }
    for(int i = 0; i < repeated->index; i++) {
        repeated->dyanmic_infos[i] = aux[i];
    }
    repeated->size += BLOCK;
    free(aux);
    return 1;
}