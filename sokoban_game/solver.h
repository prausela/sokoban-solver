#ifndef __SOLVERS__

#define __SOLVERS__

#include "game_structures.h"
#include "../search_algos/algos.h"
#include "solution_action_list.h"


/*
 * Main function to call to solve an instance of a game given an initial state.
 * Params:
 * initial_state: initial game state
 * algorithm: id of the type of search algorithm to be used
 * heuristic: heuristic function to use in the case of an informed search method
 * cost: cost function to use in case the search method needs it
 * */
solution_actions_list * solve(game_state_t * initial_state, search_algos_id_t algorithm, 
    int (*heuristic)(game_state_t*));

#endif
