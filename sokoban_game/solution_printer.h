#ifndef __SOLUTION_PRINTER__

#define __SOLUTION_PRINTER__

#include "game_structures.h"
#include "solution_action_list.h"

int print_solution(game_state_t * initial_state, solution_actions_list * actions_list);

#endif
