#ifndef __SOLUTION_ACTION_LIST__
#define __SOLUTION_ACTION_LIST__

#define INITIAL_LIST_SIZE 256

/*
 * Enum type for possible action
 * */
typedef enum action
{
    UP    = 0,
    DOWN  = 1,
    LEFT  = 2,
    RIGHT = 3,
} action_t;

/*
 * Structure for maintaining the list of actions
 * the algorithm takes while searching the game state
 * tree
 * */
typedef struct solution_actions_list_struct
{
    action_t * actions;
    int        size;
    int        index;
    long        frontier;
    long        expanded;
} solution_actions_list;

/*
 * Initializes solution_actions_list
 * */
solution_actions_list* new_solution_actions_list(int size);

/*
 * Copies a list into another one.
 * Possible use case: bfs. When expanding a node,
 * generate a new solution list, copy the expaned node's list into
 * it and append the last value.
 * */
void copy_list_into(solution_actions_list* src, solution_actions_list* dest);

void free_solution_actions_list(solution_actions_list* list);

void add_action_to_list(solution_actions_list* list, action_t action);

int remove_recent(solution_actions_list* list);

#endif
