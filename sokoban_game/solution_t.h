#ifndef __SOLUTION_T__
#define __SOLUTION_T__

#include "game_structures.h"
#include "solution_action_list.h"

/*
 * Structure that contains the game at a particular state
 * plus the actions that lead to that state from the initial game state
 * */
typedef struct solution
{
    dynamic_game_state_t  * dynamic_info;
    solution_actions_list * actions_list;
    int depth;
} solution_t;

/*
 * Applies an action to the game state contained in curr_solution and
 * returns a new solution_t that contains the updated game state and
 * an identical solution action list with action appended to it
 * */
solution_t * apply_action_to_solution(solution_t * curr_solution, static_game_state_t * static_info, action_t action);

/*
 * Initializes a solution_t structure with the given state and an empty solutions
 * list
 * */
solution_t * new_solution(dynamic_game_state_t * state, int size);



#endif
