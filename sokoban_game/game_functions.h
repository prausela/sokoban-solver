#ifndef __GAME_FUNCTIONS__
#define __GAME_FUNCTIONS__

#include "solution_action_list.h"
#include "game_structures.h"

typedef enum action_result {
    NOT_MOVED = 0,
    MOVED_BOX = 1,
    NOT_MOVED_BOX = 2,
} action_result_t;

/*
 * Applies a possible action given a game state
 * */
int apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action);

/*
 * Deapplies the action to the game state if applicable
 * */
void de_apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action, action_result_t result);

/*
 * Returns true if the action is applicable to the
 * state
 * */
int can_apply_action(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info, action_t action);

/*
 * Checks for solution condition
 * */
int is_solution(dynamic_game_state_t * dynamic_info, static_game_state_t * static_info);

#endif
