#ifndef __REPEATED_STATES__
#define __REPEATED_STATES__

#include "game_structures.h"


void initialize_repeated_states();
void clear_repeated_states();
int add_state(dynamic_game_state_t * dynamic_info);
int contains_state(dynamic_game_state_t * dynamic_info);

#endif
