#include <stdlib.h>
#include "solution_action_list.h"
#include <stdio.h>

#define BLOCK 256

static void increase_array_size(solution_actions_list * list);

solution_actions_list* new_solution_actions_list(int size) {
    solution_actions_list * ret = (solution_actions_list *)malloc(sizeof(solution_actions_list));
    ret->actions                = (action_t *)malloc(size*sizeof(action_t));
    ret->expanded               = 0;
    ret->frontier               = 0;
    ret->index                  = 0;
    ret->size                   = size;
    return ret;
}

void free_solution_actions_list(solution_actions_list* list) {
    free(list->actions);
    free(list);
}

void add_action_to_list(solution_actions_list* list, action_t action) {
    if(list->size == list->index) {
        increase_array_size(list);
    }
    list->actions[list->index] = action;
    list->index += 1;
}

static void increase_array_size(solution_actions_list * list) {
    action_t * aux = list->actions;
    list->actions  = (action_t *)malloc((list->size + BLOCK)*sizeof(action_t));
    for(int i = 0; i < list->index; i++) {
        list->actions[i] = aux[i];
    }
    list->size += BLOCK;
    free(aux);
}

int remove_recent(solution_actions_list* list) {
    int ret;
    if(list->index - 1 < 0) {
        ret = -1;
    } else {
        ret = list->actions[list->index-1];
        list->index -= 1;
    }
    return ret;
}

void copy_list_into(solution_actions_list* src, solution_actions_list* dest) {
    for(int i = 0; i < src->index; i++) {
        add_action_to_list(dest, src->actions[i]);
    }
    dest->expanded = src->expanded;
    dest->frontier = src->frontier;
    dest->size     = src->size;
    dest->index    = src->index;
}
