#include <stdlib.h>
#include "solution_t.h"
#include "game_functions.h"

solution_t * new_solution(dynamic_game_state_t * dynamic_info, int size) {
    solution_t * ret  = (solution_t *)malloc(sizeof(solution_t));
    ret->actions_list = new_solution_actions_list(size);
    ret->dynamic_info = copy_dynamic_game_state(dynamic_info);
    ret->depth = 0;
    return ret;
}

solution_t * apply_action_to_solution(solution_t * curr_solution, static_game_state_t * static_info, action_t action) {
    /*
     * Copy curr_solution and apply to the new solution
     */
    solution_t * ret  = new_solution(curr_solution->dynamic_info, curr_solution->actions_list->size + 1);

    copy_list_into(curr_solution->actions_list, ret->actions_list);

    if(apply_action(ret->dynamic_info, static_info, action)) {
        ret->depth = curr_solution->depth + 1;
        add_action_to_list(ret->actions_list, action);
    }

    return ret;

}
