#ifndef __QUEUE__
#define __QUEUE__

/*
 * Header file for queue data structure.
 * Utility for certain search algorithms
 * */
typedef struct queue_tad * queue;

queue new_queue();
void enqueue(queue q, void * value);
void * poll();
int is_empty(queue q);

#endif
