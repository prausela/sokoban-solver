#include "queue.h"
#include <stdlib.h>

typedef struct queue_node 
{
    struct queue_node * next;
    struct queue_node * prev;
    void *       value;
} queue_node;

typedef struct queue_tad 
{
    queue_node * initial_node;
    queue_node * last_node;
} queue_tad;

static queue_node * new_queue_node(void * value, queue_node * prev, queue_node * next);

queue new_queue() {
    queue ret = (queue)malloc(sizeof(queue_tad));
    ret->initial_node = NULL;
    ret->last_node    = NULL;
    return ret;
}

void enqueue(queue q, void * data) {
    if(q->initial_node == NULL) {
        q->initial_node = new_queue_node(data, NULL, NULL);
        q->last_node = q->initial_node;
        return;
    }
    q->last_node->next = new_queue_node(data, q->last_node, NULL);
    q->last_node = q->last_node->next;
}

void * poll(queue q) {
    queue_node * aux = q->initial_node;
    void * ret = aux->value;
    q->initial_node = aux->next;
    if(q->initial_node != NULL) {
        q->initial_node->prev = NULL;
    }
    free(aux);
    return ret;
}

int is_empty(queue q) {
    return q->initial_node == NULL;
}

static queue_node * new_queue_node(void * value, queue_node * prev, queue_node * next) {
    queue_node * ret = (queue_node *)malloc(sizeof(queue_node));
    ret->value = value;
    ret->prev = prev;
    ret->next = next;
    return ret;
}