#include <stdio.h>
#include "printer.h"
#include "../parsers/parsers.h"
#include "../sokoban_game/game_functions.h"

/*
 * Dynamic state variables associated with a particular
 * game state
 * 
 * typedef struct dynamic_game_state_t {
 *     int   player_row;
 *     int   player_col;
 *     int   num_boxes;
 *     int * boxes_rows;
 *     int * boxes_cols;
 * } dynamic_game_state_t;
 *
 *
 * Static state variables associated with a particular
 * game state
 * 
 * typedef struct static_game_state_t {
 *     position_state_t ** state_matrix;
 *     int                 rows;
 *     int                 cols;
 *     int                 num_objectives;
 *     int *               objective_rows;
 *     int *               objective_cols;
 * } static_game_state_t;
 *
 *
 * Main structure that represents a game state.
 * Association of a dynamic game state and a
 * static game state
 * 
 * typedef struct game_state
 * {
 *     dynamic_game_state_t * dynamic_game_state;
 *     static_game_state_t  * static_game_state;
 * } game_state_t;
 * 
 */

typedef struct
{
	game_object_type_t 	type;
	char 				printing_char;
} object_printing_footprint_t;

object_printing_footprint_t object_printing_footprint[] = {
	{ WALL_OBJ		, '#' },
    { NO_OBJ		, ' ' },
    { UNPLACED_BOX	, '$' },
    { PLACED_BOX	, '*' },
    { GOAL			, '.' },
    { PLAYER		, '@' },
};

int print_game_state(game_state_t * game_state){
	int i, j;
	position_t position;
	game_object_type_t game_object_type;
	for (i = 0; i < game_state->static_game_state->rows; i++){
		position.row = i;
		for(j = 0; j < game_state->static_game_state->cols; j++){
			position.col = j;
			if((game_object_type = get_item_at_pos(game_state, &position)) == NOT_AN_OBJECT){
				return 0;
			}
			printf(CHAR_PATTERN, object_printing_footprint[game_object_type].printing_char);
		}
		printf(CHAR_PATTERN, LINE_SEPARATOR);
	}
	return 1;
}

/*
 * typedef struct solution_actions_list_struct
 * {
 *     action_t * actions;
 *     int        size;
 *     int        index;
 *     int        frontier;
 *     int        expanded;
 * } solution_actions_list;
 */

int print_solution(game_state_t * game_state, solution_actions_list *actions){
	int i;

	printf("Nodes at frontier: %ld\n", actions->frontier);
	printf("Nodes expanded: %ld\n", actions->expanded);
	printf("Depth (actions taken): %d\n", actions->index);

	print_game_state(game_state);

	for(i = 0; i < actions->index; i++){
		apply_action(game_state->dynamic_game_state, game_state->static_game_state, (actions->actions)[i]);
		print_game_state(game_state);
	}
	return 1;
}