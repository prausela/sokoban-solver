#ifndef __PRINTER_H__
#define __PRINTER_H__

#include "../sokoban_game/game_structures.h"

int print_game_state(game_state_t * game_state);

int print_solution(game_state_t * game_state, solution_actions_list *actions);

#endif
