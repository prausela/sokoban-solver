# Sokoban Solver! #

## Description ##

This proyect aims to showcase the difference between the following graph traversal/pathfinding algorithms:

* **Informed Search**
    * Global Greedy Search
    * A*
    * Iterative Deepening A*
* **Uninformed Search**
    * Depth-First Search
    * Breadth-First Search
    * Iterative Deepening Depth-First Search

## Compilation ##
To compile the project one must execute the following command:
`
make all
`

## Running ##
To run the project one must load a board into init.conf and the algorithm in algo.conf

There is a sample board in init.conf where:

* @ is the player
* . is an objective
* \* is a placed box
* $ is an unplaced box
* \# is a wall

algo.conf's possible configurations are as follows:

### Global Greedy Search ###
algorithm   : GGS

heuristic   : X

### A* ###
algorithm   : A

heuristic   : X

### IDA* ###
algorithm   : IDA

heuristic   : X

### DFS ###
algorithm   : DFS

### BFS ###
algorithm   : BFS

### IDDFS ###
algorithm   : IDDFS



*Note:* algorithm should be followed by a tab and after the : a space. And X should be a value between 1-8.

(e.g. algorithm\t: GGS)
