#include <limits.h>
#include <stdlib.h>
#include "heuristics.h"
#include "../sokoban_game/game_functions.h"

static int cant_move_box(game_state_t * state, int box_row, int box_col) {
    static_game_state_t * static_info = state->static_game_state;
    int wall_up = wall_at(static_info, box_row - 1, box_col);
    int wall_down = wall_at(static_info, box_row + 1, box_col);
    int wall_left = wall_at(static_info, box_row, box_col - 1);
    int wall_right = wall_at(static_info, box_row, box_col + 1);
    return (wall_up || wall_down) && (wall_left || wall_right);
}

static int box_at_objective(game_state_t * game_state, int box_row, int box_col) {
    static_game_state_t * static_info = game_state->static_game_state;
    for(int obj = 0; obj < static_info->num_objectives; obj++) {
        if(static_info->objective_rows[obj] == box_row && static_info->objective_cols[obj] == box_col) {
            return 1;
        }
    }
    return 0;
}

static int closest_distance_to_box(game_state_t * game_state) {
    dynamic_game_state_t * dynamic_info = game_state->dynamic_game_state;
    int player_row = dynamic_info->player_row;
    int player_col = dynamic_info->player_col;
    int min = -1;
    for(int box = 0; box < dynamic_info->num_boxes; box++) {
        if(min == -1 || (
        abs(player_row - dynamic_info->boxes_rows[box]) + abs(player_col - dynamic_info->boxes_cols[box] < min))) {
            min = abs(player_row - dynamic_info->boxes_rows[box]) + abs(player_col - dynamic_info->boxes_cols[box] < min);
        }
    }
    return min;
}

static int closest_distance_to_objective(game_state_t * game_state, int box) {
    int box_row = game_state->dynamic_game_state->boxes_rows[box];
    int box_col = game_state->dynamic_game_state->boxes_cols[box];
    static_game_state_t * static_info = game_state->static_game_state;
    int min = -1;
    for(int obj = 0; obj < game_state->static_game_state->num_objectives; obj++) {
        if(min == -1 ||
         abs(box_row - static_info->objective_rows[obj]) + abs(box_col - static_info->objective_cols[obj]) < min) {
             min = abs(box_row - static_info->objective_rows[obj]) + abs(box_col - static_info->objective_cols[obj]);
         }
    }
    return min;
}

static int box_obstacle(game_state_t* game_state) {
    dynamic_game_state_t * dynamic_info = game_state->dynamic_game_state;
    for(int box = 0; box < dynamic_info->num_boxes; box++) {
        if(cant_move_box(game_state, dynamic_info->boxes_rows[box], dynamic_info->boxes_cols[box]) && !box_at_objective(game_state, dynamic_info->boxes_rows[box], dynamic_info->boxes_cols[box])) {
            return 1;
        }
    }
    return 0;
}

int heuristic1(game_state_t * state) {
    return closest_distance_to_box(state);
}

int heuristic2(game_state_t * state) {
    int ret = 0;
    for(int box = 0; box < state->dynamic_game_state->num_boxes; box++) {
        ret += closest_distance_to_objective(state, box);
    }
    return ret;
}

int heuristic3(game_state_t * state) {
    return heuristic1(state) + heuristic2(state);
}

int heuristic4(game_state_t * state) {
    if(box_obstacle(state)) {
        return -1;
    }
    return 1;
}
int heuristic5(game_state_t * state) {
    if(box_obstacle(state)) {
        return -1;
    }
    return heuristic2(state);
}

int heuristic6(game_state_t * state) {
    dynamic_game_state_t * dynamic_info = state->dynamic_game_state;
    int player_row = dynamic_info->player_row;
    int player_col = dynamic_info->player_col;
    int ret = 0;
    for(int box = 0; box < dynamic_info->num_boxes; box++) {
        ret += abs(player_row - dynamic_info->boxes_rows[box]) + abs(player_col - dynamic_info->boxes_cols[box]);
    }
    return ret;
}

int heuristic7(game_state_t * state) {
    return heuristic2(state) + heuristic6(state);
}

static int distances_to_obj(game_state_t * game_state, int boxes_obj_mapper[], int num_objectives) {
    int ret = 0;
    dynamic_game_state_t * dynamic_info = game_state->dynamic_game_state;
    static_game_state_t * static_info = game_state->static_game_state;
    for(int i = 0; i < num_objectives; i++) {
        ret += abs(dynamic_info->boxes_rows[i] - static_info->objective_rows[boxes_obj_mapper[i]])
                + abs(dynamic_info->boxes_cols[i] - static_info->objective_cols[boxes_obj_mapper[i]]);
    }
    return ret;
}

static void swap(int boxes_obj_mapper[], int i, int j) {
    int aux = boxes_obj_mapper[i];
    boxes_obj_mapper[i] = boxes_obj_mapper[j];
    boxes_obj_mapper[j] = aux;
}

static int rec(game_state_t * state, int boxes_obj_mapper[], int num_objectives, int curr) {
    if(curr == num_objectives - 1) {
        return distances_to_obj(state, boxes_obj_mapper, num_objectives);
    }
    int min = -1;
    int aux = 0;
    for(int i = curr; i < num_objectives; i++) {
        for(int j = i; j < num_objectives; j++) {
            swap(boxes_obj_mapper, i, j);
            aux = rec(state, boxes_obj_mapper, num_objectives, curr + 1);
            if(min == -1 || min > aux) {
                min = aux;
            }
            swap(boxes_obj_mapper, j, i);
        }
    }
    return min;
}

int heuristic8(game_state_t * state) {
    static_game_state_t * static_info = state->static_game_state;
    int boxes_obj_mapper[static_info->num_objectives];
    for(int i = 0; i < static_info->num_objectives; i++) {
        boxes_obj_mapper[i] = i;
    }
    return rec(state, boxes_obj_mapper, static_info->num_objectives, 0);
}
