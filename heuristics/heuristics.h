#ifndef __HEURISTIC__
#define __HEURISTIC__

#include "../sokoban_game/game_structures.h"

int heuristic1(game_state_t * state);
int heuristic2(game_state_t * state);
int heuristic3(game_state_t * state);
int heuristic4(game_state_t * state);
int heuristic5(game_state_t * state);
int heuristic6(game_state_t * state);
int heuristic7(game_state_t * state);
int heuristic8(game_state_t * state);

#endif