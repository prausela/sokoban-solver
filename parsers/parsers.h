#ifndef __PARSERS_H__

#define __PARSERS_H__

#define LINE_FORMAT(__KW, __PATTERN)    __KW "\t" KEY_VALUE_SEP " " __PATTERN "\n" 
#define STRING_PATTERN                  "%s"  
#define CHAR_PATTERN					"%c"  
#define INT_PATTERN                     "%d"
#define FILE_ACCESS                     "r"

#define KEY_VALUE_SEP                   ":"
#define LINE_SEPARATOR					'\n'

#endif
