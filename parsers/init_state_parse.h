#ifndef __INIT_STATE_H__
#define __INIT_STATE_H__

#include "../sokoban_game/game_structures.h"

typedef enum parse_init_file_issues {
    INIT_NO_ISSUES                 = 0,
    INIT_FILE_CANNOT_BE_OPENED,
    INIT_FILE_CANNOT_GET_POS,
    INIT_FILE_CANNOT_SET_POS,
    INIT_BAD_ARGUMENTS,
    INIT_INVALID_CHAR,
} parse_init_file_issues_t;

int parse_init_state(char * path, game_state_t * game_state);

#endif
