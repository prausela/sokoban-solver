#ifndef __ALGO_PARSER_H__

#define __ALGO_PARSER_H__

#include "../search_algos/algos.h"

typedef enum parse_algo_file_issues {
    NO_ISSUES                 = 0,
    FILE_CANNOT_BE_OPENED,
    SCANF_FAILED,
    SCANF_NO_MATCH,
    UNDEFINED_ALGORITHM,
    UNDEFINED_HEURISTIC,
    UNDEFINED_LIMIT,
} parse_algo_file_issues_t;

/*
 * Function that reads a configuration file and extracts the algorithm desired
 * and parameters needed for it.
 * 
 *      INPUT   -> path: where the configuration file is located
 * 
 *      OUTPUT  -> algo_params: parameters needed for algorithm in config file.
 *                      one can check search_algos_def array to see which are them.
 * 
 *      SAMPLE CORRECT FILES (begins with no tab, FORMAT "str\t: str\n" per line):
 * 
 *          algorithm   : GGS\n
 *          heuristic   : 1\n
 * 
 *          algorithm   : A\n
 *          heuristic   : 3\n
 * 
 *          algorithm   : IDA\n
 *          heuristic   : 2\n
 *          limit       : 100\n
 * 
 *          algorithm   : DFS\n
 *          
 *          algorithm   : BFS\n
 * 
 *          algorithm   : IDDFS\n
 *          limit       : 2000\n
 * 
 */


int parse_algo_file(char * path, algo_params_t * algo_params);

#endif
