#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include "algo_parser.h"
#include "parsers.h"

typedef enum algo_param_types {
    HEURISTIC,
    LIMIT,
} algo_param_types_t;

typedef struct search_algos {
    char *                  name;
    search_algos_id_t       id;
    algo_param_types_t *    params;
    int                     params_amount;
} search_algos_t;

algo_param_types_t ggs_params[]   = { HEURISTIC };
algo_param_types_t a_params[]     = { HEURISTIC };
algo_param_types_t ida_params[]   = { HEURISTIC };
//algo_param_types_t iddfs_params[] = { NULL };

search_algos_t search_algos_def[] ={
    { "GGS",    GGS,    ggs_params, 1 },
    { "A",      A,      a_params, 1 },
    { "IDA",    IDA,    ida_params, 1 },
    { "DFS",    DFS,      NULL, 0 },
    { "BFS",    BFS,      NULL, 0 },
    { "IDDFS",  IDDFS,    NULL, 0 }
};

#define ALGORITH_KW                     "algorithm"
#define HEURISTIC_KW                    "heuristic"
#define LIMIT_KW                        "limit"
#define MAX_ALGO_LENGTH                 6 //5 plus null terminator

/*
 * Function for evaluating if scanf was succesful at retriving the information needed.
 * 
 *      INPUT   ->  matched: returned integer from scanf function
 *                 
 *      OUTPUT  ->  SCANF_FAILED (if EOF),
 *                  SCANF_NO_MATCH (if 0 which is nothing was read),
 *                  NO_ISSUES (if something was read)
 */

static inline int 
invalid_match(int matched){
    if (matched == EOF) {
        return SCANF_FAILED;
    } else if (matched == 0){
        return SCANF_NO_MATCH;
    }
    return NO_ISSUES;
}

/*
 * Function for setting the algorithm's parameters (heuristic and limit).
 * Given an algorithm id from the algorithm definitions it iterates over
 * its parameters (which are needed for that specific algorithm) and sets them
 * acording to the lines it reads from the file. The order specified in the params
 * array is the one they should be placed on the configuration file.
 * 
 *      INPUT   ->  file: file from which to read the parameter lines,
 *                          the pointer should be placed at the beginning
 *                          of the line with the first parameter.
 *                  algo_id : which algorithm are we parsing.
 *                          depending on it the parameters vary.
 *                          one can check the seach_algos_def and see
 *                          which are which.
 *                 
 *      OUTPUT  ->  UNDEFINED_HEURISTIC (if a heuristic was required but failed to be read from file)
 *                  UNDEFINED_LIMIT (if a limit was required but failed to be read from file)
 *                  NO_ISSUES (if all parameters needed were set successfully)
 * 
 *                  algo_params: heuristic and limit are set if required
 *
 *      SAMPLE CORRECT LINES:
 * 
 *                  heuristic   : 3
 *                  limit       : 100
 */

static inline int
set_params(FILE * file, search_algos_id_t algo_id, algo_params_t * algo_params){
    int issue;
    if (search_algos_def[algo_id].params != NULL) {
        //int param_val;
        for (int param = 0; param < search_algos_def[algo_id].params_amount; param++) {

            if          (search_algos_def[algo_id].params[param] == HEURISTIC){

                int matched = fscanf(file, LINE_FORMAT(HEURISTIC_KW, INT_PATTERN), &(algo_params->heuristic));
                if((issue = invalid_match(matched)))
                    return UNDEFINED_HEURISTIC;

            } else if   (search_algos_def[algo_id].params[param] == LIMIT) {

                int matched = fscanf(file, LINE_FORMAT(LIMIT_KW, INT_PATTERN), &(algo_params->limit));
                if((issue = invalid_match(matched)))
                    return UNDEFINED_LIMIT;

            }

        }
    }
    //printf("Chau\n");
    return NO_ISSUES;
}

/*
 * Function for getting the algorithm id based on the text for algorithm in the file.
 * If non match, UNDEFINED_ALGORITHM is returned. Otherwise one can check the algo_id parameter.
 * 
 *      INPUT   ->  algo: text retrieved from the algorithm clause in the configuration file
 *                 
 *      OUTPUT  ->  UNDEFINED_ALGORITHM (if there is no algorithm by that acronym),
 *                  NO_ISSUES (if an algorithm matched)
 *  
 *                  algo_id : returns the id of the matching algorithm acronym.                
 */

static inline int
get_algo_id(search_algos_id_t * algo_id, char * algo){
    *algo_id = 0;
    while (*algo_id < ALGO_AMOUNT && strcmp(search_algos_def[*algo_id].name, algo) != 0) {
        (*algo_id)++;
    }
    if (*algo_id >= ALGO_AMOUNT) {
        return UNDEFINED_ALGORITHM;
    }
    return NO_ISSUES;
}

/*
 * Function that reads a configuration file and extracts the algorithm desired
 * and parameters needed for it.
 * 
 *      INPUT   -> path: where the configuration file is located
 * 
 *      OUTPUT  -> algo_params: parameters needed for algorithm in config file.
 *                      one can check search_algos_def array to see which are them.
 * 
 *      SAMPLE CORRECT FILES (begins with no tab, FORMAT "str\t: str\n" per line):
 * 
 *          algorithm   : GGS\n
 *          heuristic   : 1\n
 * 
 *          algorithm   : A\n
 *          heuristic   : 3\n
 * 
 *          algorithm   : IDA\n
 *          heuristic   : 2\n
 *          limit       : 100\n
 * 
 *          algorithm   : DFS\n
 *          
 *          algorithm   : BFS\n
 * 
 *          algorithm   : IDDFS\n
 *          limit       : 2000\n
 * 
 */

int parse_algo_file(char * path, algo_params_t * algo_params){
    int issue;

    FILE * file = fopen(path, FILE_ACCESS);
    if (file == NULL) {
        return FILE_CANNOT_BE_OPENED;
    }

    char algo[MAX_ALGO_LENGTH];
    int matched = fscanf(file, LINE_FORMAT(ALGORITH_KW, STRING_PATTERN), algo);

    if((issue = invalid_match(matched)) != NO_ISSUES)
        return UNDEFINED_ALGORITHM;

    if((issue = get_algo_id(&(algo_params->algo_id), algo)) != NO_ISSUES)
        return issue;

    if((issue = set_params(file, algo_params->algo_id, algo_params)) != NO_ISSUES)
        return issue;

    fclose(file);

    return NO_ISSUES;
}
