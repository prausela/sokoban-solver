#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "init_state_parse.h"
#include "parsers.h"

/*
 * typedef enum position_state
 * {
 *     FREE  = 0,
 *     WALL  = 1,
 *     BOX = 2,
 * } position_state_t;

 * typedef struct game_state
 * {
 *     position_state_t** state_matrix;
 *     int                rows;
 *     int                cols;
 *     int                player_row;
 *     int                player_col;
 *     int*               objective_rows;
 *     int*               objective_cols;
 *     int                num_objectives;
 * } game_state_t;
 */

typedef struct {
    char                symbol;
    game_object_type_t  game_object_type;
    int                 (*action)(game_state_t *, position_t *, game_object_type_t);
} game_object_mapper_t;

game_object_mapper_t game_object_mapper[] = {
    {' '            , NO_OBJ        , place_in_board      },
    {'#'            , WALL_OBJ      , place_in_board      },
    {'$'            , UNPLACED_BOX  , add_box             },
    {'*'            , PLACED_BOX    , add_box             },
    {'.'            , GOAL          , add_objective       },
    {'@'            , PLAYER        , add_player          },
    {LINE_SEPARATOR , NOT_AN_OBJECT , move_board_next_row },
};

#define GAME_OBJECT_MAPPER_AMOUNT 7

static inline int board_dimensions(FILE * board_file, dimensions_t * dimensions){
    if(dimensions == NULL)
        return 0;
    dimensions->height     = 0;
    dimensions->width      = 0;
    dimensions->boxes      = 0;
    dimensions->objectives = 0;
    int c, curr_width      = 0, curr_height = 1, curr_boxes = 0, curr_objectives = 0;
    while((c = fgetc(board_file)) != EOF){
        if(c == LINE_SEPARATOR){
            if(curr_width > dimensions->width){
                dimensions->width = curr_width;
            }
            curr_width = 0;
            curr_height++;
        } else {
            if(c == '$' || c == '*'){
                curr_boxes++;
            }
            if(c == '.' || c == '*'){
                curr_objectives++;
            }
            curr_width++;
        }
    }
    dimensions->height     = curr_height;
    dimensions->boxes      = curr_boxes;
    dimensions->objectives = curr_objectives;
    return INIT_NO_ISSUES;
}

static inline int fill_board(FILE * board_file, game_state_t * game_state){
    int c, i, issue, matched = 0;
    int game_objects_amount = GAME_OBJECT_MAPPER_AMOUNT;
    position_t curr_position = {0, 0};
    while((c = fgetc(board_file)) != EOF){
        for(i = 0; i < game_objects_amount; i++){
            if(game_object_mapper[i].symbol == c){
                matched = 1;
                if((issue = game_object_mapper[i].action(game_state, &curr_position, game_object_mapper[i].game_object_type)) != BOARD_INIT_OK)
                    return issue;
            }
        }
        if(!matched){
            return INIT_INVALID_CHAR;
        }
        matched = 0;
    }
    return INIT_NO_ISSUES;
}

int parse_init_state(char * path, game_state_t * game_state){
    if(game_state == NULL)
        return INIT_BAD_ARGUMENTS;

    FILE * file = fopen(path, FILE_ACCESS);
    if (file == NULL) {
        return INIT_FILE_CANNOT_BE_OPENED;
    }
    int issue;

    dimensions_t dimensions;
    if((issue = board_dimensions(file, &dimensions)) != INIT_NO_ISSUES)
        return issue;

    if((issue = init_board(game_state, &dimensions)) != BOARD_INIT_OK)
        return issue;

    fclose(file);

    file = fopen(path, FILE_ACCESS);
    if (file == NULL) {
        return INIT_FILE_CANNOT_BE_OPENED;
    }

    if((issue = fill_board(file, game_state)) != INIT_NO_ISSUES)
        return issue;
    
    fclose(file);

    return INIT_NO_ISSUES;
}
