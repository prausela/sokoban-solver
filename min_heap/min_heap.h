#ifndef __MIN_HEAP__
#define __MIN_HEAP__

#include <stdbool.h>

typedef struct min_heap * min_heap;

min_heap new_heap(int(*function)(void *));
int 	 add(min_heap heap, void * item);
void *   pop(min_heap heap);
bool	 heap_is_empty(min_heap heap);

#endif
