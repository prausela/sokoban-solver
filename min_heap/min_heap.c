#include <stdlib.h>
#include <stdbool.h>
#include "min_heap.h"
#include <math.h>

#define BLOCK 256

typedef struct min_heap {
    void ** array;
    size_t size;
    size_t index;
    int (*function)(void *);
} min_heap_t;


static inline
int    biggen_heap(min_heap_t *heap);

static inline
void   min_heapify_child_to_parent(min_heap_t *heap, long index);

static inline
void   min_heapify_parent_to_child(min_heap_t *heap, long index);

static inline
void   swap(void ** array, long index1, long index2);

static inline
bool   has_node(min_heap_t *heap, long index);

static inline
int   if_smaller_swap(min_heap_t *heap, long child, long parent);

static inline
int    compare(min_heap_t *heap, void *node1, void *node2);

static inline
long    get_parent(long index);

static inline
long    get_left_child(long index);

static inline
long    get_right_child(long index);


min_heap_t* new_heap(int (*function)(void *)) {
    min_heap_t *ret    = (min_heap_t *)malloc(sizeof(min_heap_t));
    ret->array         = (void**)malloc(BLOCK*sizeof(void*));
    ret->size          = BLOCK;
    ret->index         = 0;
    ret->function      = function;
    return ret;
}

int add(min_heap_t *heap, void * item) {
    if(heap->function(item) == -1) {
        return -1;
    }

    if(heap->index == 0) {
        heap->index   += 1;
        heap->array[0] = item;
        return 0;
    }

    /*
     * Check for space
     */

    if(heap->index >= heap->size) {
        if(!biggen_heap(heap))
            return -2;
    }

    /*
     * Add an item in the last possible index
     * and rearrange heap for it to have min as parent.
     */

    heap->array[heap->index] = item;
    heap->index += 1;
    min_heapify_child_to_parent(heap, get_parent(heap->index));
    return 1;
}

static inline int   biggen_heap(min_heap_t *heap) {
    void ** aux = heap->array;
    heap->array = (void**)malloc((heap->size + BLOCK)*sizeof(void *));
    for(int i = 0; i < heap->index; i++) {
        heap->array[i] = aux[i];
    }
    free(aux);
    //void ** aux = realloc(heap->array, ((heap->size)+BLOCK)*sizeof(void *));
    //if(aux == NULL)
    //   return 0;
    //heap->array = aux;
    heap->size = (heap->size)+BLOCK;
    return 1;
}

static inline void  min_heapify_child_to_parent(min_heap_t *heap, long parent) {
    long left_child, right_child;
    while (parent >= 0) {

        /*
         * Get children
         */

        left_child  = get_left_child(parent);
        right_child = get_right_child(parent);

        /*
         * Set the smallest node as parent
         */

        if(has_node(heap, left_child)){
            if_smaller_swap(heap, left_child, parent);
        }
        if(has_node(heap, right_child)){
            if_smaller_swap(heap, right_child, parent);
        }

        /*
         * Repeat procedure with parent's parent.
         */

        parent      = get_parent(parent);
    }
}

static inline long get_left_child(long index){
    return (2*index) + 1;
}

static inline long get_right_child(long index){
    return (2*index) + 2;
}

static inline long get_parent(long index){
    return floor((index - 1)/(double)2);
}

static inline bool   has_node(min_heap_t *heap, long index){
    return index >= 0 && index < heap->index;
}


static inline int   if_smaller_swap(min_heap_t *heap, long child, long parent){
    if(compare(heap, heap->array[parent], heap->array[child]) > 0){
        swap(heap->array, parent, child);
        return 1;
    }
    return 0;
}

static inline int    compare(min_heap_t *heap, void *node1, void *node2){

    /*
     * A NULL node should always be at the bottom of the heap.
     * (It is bigger than any other).
     */

    if(node1 == NULL && node2 == NULL)
        return 0;
    if(node1 == NULL)
        return 1;
    if(node2 == NULL)
        return -1;
    /*
     * A node is bigger if their function value is bigger
     */

    int function_node1, function_node2;
    function_node1 = heap->function(node1);
    function_node2 = heap->function(node2);
    if(function_node1 == -1 && function_node2 == -1) {
        return 0;
    }
    if(function_node1 == -1) {
        return -1;
    }
    if(function_node2 == -1) {
        return 1;
    }
    return function_node1 - function_node2;

}
static inline void   swap(void ** array, long index1, long index2) {
    void * aux    = array[index1];
    array[index1] = array[index2];
    array[index2] = aux;
}

void *pop(min_heap_t *heap){

    /*
     * If there are no elements return NULL.
     */


    if (heap->index == 0)
        return NULL;

    /*
     * We will return the first element which is the minimum.
     */

    void *ret       = heap->array[0];

    (heap->index)--;
    if(heap->index != 0) {
        heap->array[0] = heap->array[heap->index];
        min_heapify_parent_to_child(heap, 0);
    }

    /*
     * We remove the first element from the heap.
     */

    //heap->array[0]  = heap->array[heap->index-1];

    /*
     * We choose the new fist element by
     */
    //min_heapify_parent_to_child(heap, 0);
    //(heap->index)--;

    return ret;
}

static inline void  min_heapify_parent_to_child(min_heap_t *heap, long parent) {
    long left_child, right_child;

    /*
     * Get children
     */

    left_child  = get_left_child(parent);
    right_child = get_right_child(parent);

    /*
     * Set the smallest node as parent. And min heapify child if its parent was bigger.
     */

    if(has_node(heap, right_child)){
        if(if_smaller_swap(heap, right_child, parent)){
            min_heapify_parent_to_child(heap, right_child);
        }
    }
    if(has_node(heap, left_child)){
        if(if_smaller_swap(heap, left_child, parent)){
            min_heapify_parent_to_child(heap, left_child);
        }
    }


}

bool heap_is_empty(min_heap_t *heap){
    if(heap == NULL)
        return true;
    return heap->index == 0;
}
