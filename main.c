//#include "parsers/algo_parser.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <limits.h>
#include "search_algos/algos.h"
#include "parsers/init_state_parse.h"
#include "parsers/algo_parser.h"
#include "search_algos/algos.h"
#include "printer/printer.h"
#include "sokoban_game/solver.h"
#include "sokoban_game/game_structures.h"
#include "heuristics/heuristics.h"

/*
 * typedef struct algo_params
 * {
 *    search_algos_id_t       algo_id;
 *    int                     heuristic;
 *    int                     limit;
 * } algo_params_t;
 *
 */

typedef int (*heuristic)(game_state_t *);

static heuristic heuristic_chooser(int id);

int main(){
    game_state_t game_state;
    algo_params_t algo_params;
    solution_actions_list *solution_actions;

    /*
     * Parse the configuration file and obtain game parameters
     */

    parse_init_state("init.conf", &game_state);
    parse_algo_file("algo.conf", &algo_params);
    printf("algo_id: %d\nheuristic: %d\n", algo_params.algo_id, algo_params.heuristic);
    /*
     * Solve the game
     */
    dynamic_game_state_t * aux = copy_dynamic_game_state(game_state.dynamic_game_state);
    clock_t start = clock();
    solution_actions = solve(&game_state, algo_params.algo_id, heuristic_chooser(algo_params.heuristic));
    clock_t end = clock();
    clock_t elapsed_time = (double)(end - start)/CLOCKS_PER_SEC;
    if(solution_actions->index == 0) {
        printf("No solution reached!\n");
    } else {
        printf("Elapsed time in seconds: %ld\n", elapsed_time);
        game_state.dynamic_game_state = aux;
        print_solution(&game_state, solution_actions);
    }

    return 0;
}

static heuristic heuristic_chooser(int id) {
    if(id == 0) {
        return NULL;
    }
    heuristic functions[] = {&heuristic1, &heuristic2, &heuristic3, &heuristic4, &heuristic5, &heuristic6, &heuristic7, &heuristic8};
    return functions[id - 1];
}
