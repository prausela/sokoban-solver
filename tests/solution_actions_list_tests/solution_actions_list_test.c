#include <stdio.h>
#include <assert.h>
#include "../../sokoban_game/solution_action_list.h"
#include "solution_actions_list_test.h"

#define TEST_SIZE 2

void test_can_add_actions_to_list() {
    /*
     * Setup
     * */
    solution_actions_list * list = new_solution_actions_list(TEST_SIZE);

    /*
     * Apply
     * */
    add_action_to_list(list, UP);
    add_action_to_list(list, DOWN);
    add_action_to_list(list, RIGHT);

    /*
     * Assert
     * */
    assert(list->actions[0] == UP);
    assert(list->actions[1] == DOWN);
    assert(list->actions[2] == RIGHT);
    assert(list->size > TEST_SIZE);

    /*
     * Free up structures
     * */
    free_solution_actions_list(list);

}

void test_can_copy_actions_list() {
    /*
     * Setup
     * */
    solution_actions_list * list1 = new_solution_actions_list(TEST_SIZE);
    solution_actions_list * list2 = new_solution_actions_list(TEST_SIZE);
    add_action_to_list(list1, UP);
    add_action_to_list(list1, DOWN);

    /*
     * Apply
     * */
    copy_list_into(list1, list2);

    /*
     * Assert
     * */
    assert(list1->actions[0] == list2->actions[0]);
    assert(list1->actions[1] == list2->actions[1]);
    assert(list1->frontier == list2->frontier);
    assert(list1->expanded == list2->expanded);

    /*
     * Free up structures
     * */
    free_solution_actions_list(list1);
    free_solution_actions_list(list2);
}
