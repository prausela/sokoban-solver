#ifndef SOKOBAN_SOLVER_SOLUTION_ACTIONS_LIST_TEST_H
#define SOKOBAN_SOLVER_SOLUTION_ACTIONS_LIST_TEST_H

void test_can_add_actions_to_list();
void test_can_copy_actions_list();

#endif
