#include <stdio.h>
#include "solution_actions_list_test.h"

int main(void) {
    printf("Testing for adding more actions than initially available size\n");
    test_can_add_actions_to_list();
    printf("Test successful\n");
    printf("***\n");
    printf("Testing for copying a list of actions into another one\n");
    test_can_copy_actions_list();
    printf("Test successful\n");
    printf("***\n");
}
