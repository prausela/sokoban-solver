#include <stdio.h>
#include "game_function_tests.h"

int main(void) {
    printf("Testing for player moving a box\n");
    printf("Right\n");
    test_player_can_move_box_right();
    printf("Down\n");
    test_player_can_move_box_down();
    printf("Up\n");
    test_player_can_move_box_up();
    printf("Left\n");
    test_player_can_move_box_left();
    printf("Test successful\n");
    printf("***\n");
    printf("Testing for player moving a box and then de-applying the action\n");
    player_can_move_box_and_then_de_apply_action();
    printf("Test successful\n");
    printf("***\n");
    printf("Testing for player-wall collision\n");
    test_player_collides_with_wall();
    printf("Test successful\n");
}
