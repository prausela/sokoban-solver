#ifndef __SOKOBAN_SOLVER_GAME_FUNCTION_TEST_H__
#define __SOKOBAN_SOLVER_GAME_FUNCTION_TEST_H__

void test_player_can_move_box_right();
void test_player_can_move_box_down();
void test_player_can_move_box_left();
void test_player_can_move_box_up();
void test_player_collides_with_wall();
void player_can_move_box_and_then_de_apply_action();

#endif
