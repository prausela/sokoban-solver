#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "../../sokoban_game/game_structures.h"
#include "../../sokoban_game/game_functions.h"
#include "game_function_tests.h"

#define TEST_DIM 5

/*
 * Returns a game state of dimension "dimension" with all positions set
 * to FREE
 * */
static game_state_t * get_test_game_state(int dimension);
static void assert_player_at(game_state_t * game_state, int row, int col);
static void assert_box_at(game_state_t * game_state, int box, int row, int col);
static void free_game_state(game_state_t * game_state);

void test_player_can_move_box_right() {
    /*
     * Setup
     * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);
    int boxes_rows[] = {0};
    int boxes_cols[] = {1};
    game_state->dynamic_game_state->boxes_rows = boxes_rows;
    game_state->dynamic_game_state->boxes_cols = boxes_cols;
    game_state->dynamic_game_state->num_boxes  = 1;

    /*
     * Apply
     * */
     apply_action(game_state->dynamic_game_state, game_state->static_game_state, RIGHT);

     /*
      * Assert
      * */
     assert_player_at(game_state, 0, 1);
     assert_box_at(game_state, 0, 0, 2);

     /*
      * Free up structures
      * */
     free_game_state(game_state);
}

void test_player_can_move_box_left() {
    /*
     * Setup
     * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);
    int boxes_rows[] = {0};
    int boxes_cols[] = {1};
    game_state->dynamic_game_state->boxes_rows = boxes_rows;
    game_state->dynamic_game_state->boxes_cols = boxes_cols;
    game_state->dynamic_game_state->num_boxes  = 1;
    game_state->dynamic_game_state->player_row = 0;
    game_state->dynamic_game_state->player_col = 2;

    /*
     * Apply
     * */
    apply_action(game_state->dynamic_game_state, game_state->static_game_state, LEFT);

    /*
     * Assert
     * */
    assert_player_at(game_state, 0, 1);
    assert_box_at(game_state, 0, 0, 0);

    /*
     * Free up structures
     * */
    free_game_state(game_state);
}

void test_player_can_move_box_up() {
    /*
     * Setup
     * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);
    int boxes_rows[] = {1};
    int boxes_cols[] = {1};
    game_state->dynamic_game_state->boxes_rows = boxes_rows;
    game_state->dynamic_game_state->boxes_cols = boxes_cols;
    game_state->dynamic_game_state->num_boxes  = 1;
    game_state->dynamic_game_state->player_row = 2;
    game_state->dynamic_game_state->player_col = 1;

    /*
     * Apply
     * */
    apply_action(game_state->dynamic_game_state, game_state->static_game_state, UP);

    /*
     * Assert
     * */
    assert_player_at(game_state, 1, 1);
    assert_box_at(game_state, 0, 0, 1);

    /*
     * Free up structures
     * */
    free_game_state(game_state);
}

void test_player_can_move_box_down() {
    /*
     * Setup
     * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);
    int boxes_rows[] = {1};
    int boxes_cols[] = {1};
    game_state->dynamic_game_state->boxes_rows = boxes_rows;
    game_state->dynamic_game_state->boxes_cols = boxes_cols;
    game_state->dynamic_game_state->num_boxes  = 1;
    game_state->dynamic_game_state->player_row = 0;
    game_state->dynamic_game_state->player_col = 1;

    /*
     * Apply
     * */
    apply_action(game_state->dynamic_game_state, game_state->static_game_state, DOWN);

    /*
     * Assert
     * */
    assert_player_at(game_state, 1, 1);
    assert_box_at(game_state, 0, 2, 1);

    /*
     * Free up structures
     * */
    free_game_state(game_state);
}

void player_can_move_box_and_then_de_apply_action() {
    /*
    * Setup
    * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);
    int boxes_rows[] = {0};
    int boxes_cols[] = {1};
    game_state->dynamic_game_state->boxes_rows = boxes_rows;
    game_state->dynamic_game_state->boxes_cols = boxes_cols;
    game_state->dynamic_game_state->num_boxes  = 1;

    /*
     * Apply
     * */
    apply_action(game_state->dynamic_game_state, game_state->static_game_state, RIGHT);
    de_apply_action(game_state->dynamic_game_state, game_state->static_game_state, RIGHT);

    /*
     * Assert
     * */
    assert_player_at(game_state, 0, 0);
    assert_box_at(game_state, 0, 0, 1);

    /*
     * Free up structures
     * */
    free_game_state(game_state);
}

void test_player_collides_with_wall() {
    /*
     * Setup
     * */
    game_state_t * game_state = get_test_game_state(TEST_DIM);

    /*
     * Apply
     * */
    int could_move_up = apply_action(game_state->dynamic_game_state, game_state->static_game_state, UP);
    int could_move_left = apply_action(game_state->dynamic_game_state, game_state->static_game_state, LEFT);
    game_state->dynamic_game_state->player_row = 4;
    game_state->dynamic_game_state->player_col = 4;
    int could_move_right = apply_action(game_state->dynamic_game_state, game_state->static_game_state, RIGHT);
    int could_move_down = apply_action(game_state->dynamic_game_state, game_state->static_game_state, DOWN);

    /*
     * Assert
     * */
    assert(!could_move_up && !could_move_left
            && !could_move_right && !could_move_down);

    /*
     * Free up structures
     * */
    free_game_state(game_state);

}

static void assert_player_at(game_state_t * game_state, int row, int col) {
    assert(game_state->dynamic_game_state->player_row == row
           && game_state->dynamic_game_state->player_col == col);
}

static void assert_box_at(game_state_t * game_state, int box, int row, int col) {
    assert(game_state->dynamic_game_state->boxes_rows[box] == row
           && game_state->dynamic_game_state->boxes_cols[box] == col);
}

static game_state_t * get_test_game_state(int dimension) {
    static_game_state_t  * static_info  = (static_game_state_t *)malloc(sizeof(static_game_state_t));
    dynamic_game_state_t * dynamic_info = (dynamic_game_state_t *)malloc(sizeof(static_game_state_t));
    game_state_t         * game_state   = (game_state_t *)malloc(sizeof(game_state_t));

    static_info->state_matrix = (position_state_t **)malloc(dimension*sizeof(position_state_t*));
    for(int i = 0; i < dimension; i++) {
        static_info->state_matrix[i] = (position_state_t *)malloc(dimension*sizeof(position_state_t));
    }

    for(int i = 0; i < dimension; i++) {
        for(int j = 0; j < dimension; j++) {
            static_info->state_matrix[i][j] = FREE;
        }
    }

    static_info->num_objectives = 0;
    static_info->rows           = dimension;
    static_info->cols           = dimension;
    static_info->objective_cols = NULL;
    static_info->objective_rows = NULL;

    dynamic_info->player_col = 0;
    dynamic_info->player_row = 0;
    dynamic_info->boxes_cols = NULL;
    dynamic_info->boxes_rows = NULL;

    game_state->static_game_state = static_info;
    game_state->dynamic_game_state = dynamic_info;

    return game_state;

}

static void free_game_state(game_state_t * game_state) {
    for(int i = 0; i < game_state->static_game_state->rows; i++) {
        free(game_state->static_game_state->state_matrix[i]);
    }
    free(game_state->static_game_state->state_matrix);
    free(game_state->static_game_state);
    free(game_state->dynamic_game_state);
    free(game_state);
}
