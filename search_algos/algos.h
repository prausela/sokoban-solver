#ifndef __ALGOS_H__

#define __ALGOS_H__

/*
 * Enum type for search algorithms
 * */
typedef enum search_algos_id {
    GGS     = 0,
    A       = 1,
    IDA     = 2,
    DFS     = 3,
    BFS     = 4,
    IDDFS   = 5,
} search_algos_id_t;

#define ALGO_AMOUNT         6

typedef struct algo_params
{
    search_algos_id_t       algo_id;
    int                     heuristic;
    int                     limit;
} algo_params_t;

#endif
